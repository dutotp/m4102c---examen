#include <boost/asio.hpp>
#include <iostream>
#include <thread> // pour le sleep à la fin
#include <chrono> // pour le sleep à la fin

int main(int argc, char* argv[])
{

  if (argc != 2)
  {
    std::cerr << "Usage : " << argv[0] << " N°ETAGE" << std::endl;
    exit(0);
  }
 
  int etage = static_cast<int>(std::stoul(argv[1]));

  // Ajoutez ici la connection au serveur 127.0.0.1 sur le port 4567
  // Puis l'envoi de l'étage demandé
  boost::asio::io_service service_es;
  boost::asio::ip::tcp::socket sock(service_es);
  boost::asio::ip::address_v4 adresse = boost::asio::ip::address_v4::from_string("127.0.0.1");
  boost::asio::ip::tcp::endpoint point_distant(adresse,4567);
  sock.connect(point_distant);

  boost::asio::write(sock, boost::asio::buffer(&etage, sizeof(etage)));

  // Petite temporisation avant la fin du processus, pour laisser
  // au serveur le temps de couper la connection de son côté.
  // On peut faire plus élégant, mais cela nécessite votre code
  // ou cela vous apporterai trop d'aide pour la partie réseau.
  std::this_thread::sleep_for(std::chrono::milliseconds(10));

  return 0;
}
