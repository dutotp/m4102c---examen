#include <iostream>
#include <thread>
#include <chrono>
#include "Circulation.hpp"
#include "Serveur.hpp"

using namespace std;
using namespace std::chrono;

Circulation::Circulation(Partage& commun_) :
  commun(commun_), etage(0), direction(arret)
{
  for(int i = 0; i <= ETAGE_MAX; i++)
    {
      etage_demande[i] = false;
    }
}

bool Circulation::verif_mouvement(int debut, int fin)
{
  bool mouvement = false;
  
  for(int i = debut; i <= fin; i++)
    {
      mouvement = mouvement || etage_demande[i];
    }
  
  return mouvement;
}

void Circulation::operator()(void)
{
  // L'attribut privé "commun" ne sert que pour la seconde partie, avec prise en compte de la synchronisation.
  (void) commun;
  
  while(true) {

    // PREMIERE ACTION :
    // teste si on a besoin d'ouvrir à l'étage actuel
    // ouvre les portes deux secondes si nécessaire
    
    {
      unique_lock<mutex> verrou(commun.tableau);
      
      if(etage_demande[etage])
	{
	  verrou.unlock();
	  commun.affichage.lock();
	  cout << "Actuellement à l'étage : " << etage << endl;
	  commun.affichage.unlock();
	  this_thread::sleep_for(milliseconds(100));
	  commun.affichage.lock();
	  cout << "Ouverture des portes à l'étage " << etage << endl;
	  commun.affichage.unlock();
	  this_thread::sleep_for(milliseconds(2000));
	  commun.affichage.lock();
	  cout << "Fermeture des portes à l'étage " << etage << endl;
	  commun.affichage.unlock();
	  verrou.lock();
	}
      etage_demande[etage] = false;
    }

    // DEUXIEME ACTION :
    // teste si l'on doit continuer le mouvement précédent
    // sinon on teste si l'on doit changer de sens
    // en dernier recours l'ascenseur s'arrête en attente

    {
      lock_guard<mutex> verrou(commun.affichage);
      lock_guard<mutex> verrou2(commun.tableau);
    cout << "Actuellement à l'étage : " << etage;
      
    bool mouvement = false;
    
    switch(direction) {

    case montee:
      mouvement = verif_mouvement(etage+1,ETAGE_MAX);
      if (mouvement) { // pas de changement de sens
	break;
      }
      mouvement = verif_mouvement(0,etage-1);
      if (mouvement) { // changement de sens
	direction = descente;
	break;
      }
      direction = arret; // mise à l'arrêt	
      break;

    case descente:
      mouvement = verif_mouvement(0,etage-1);
      if (mouvement) { // pas de changement de sens
	break;
      }
      mouvement = verif_mouvement(etage+1,ETAGE_MAX);
      if (mouvement) { // changement de sens
	direction = montee;
	break;
      }
      direction = arret; // mise à l'arrêt	
      break;

    default:
      mouvement = verif_mouvement(0,etage-1);
      if (mouvement) { // mise en descente
	direction = descente;
	break;
      }
      mouvement = verif_mouvement(etage+1,ETAGE_MAX);
      if (mouvement) { // mise en montée
	direction = montee;
	break;
      }
      direction = arret; // mise à l'arrêt	
      break;

    }
    
    switch(direction) {
      
    case montee:
      cout << " en montée." << endl;
      etage++;
      break;

    case descente:
      cout << " en descente." << endl;
      etage--;
      break;
      
    default:
      cout << " à l'arrêt." << endl;
    }
    } // lock_guard commun.affichage et commun.tableau

    // TROISIEME ACTION :
    // Si l'on doit s'arrêter, le faire réellement pour ne pas avoir
    // des messages "à l'arrêt" en boucle toutes les demi secondes

    // Cette première temporisation a deux rôles :
    // 1. simuler le déplacement de l'ascenseur
    // 2. éviter de boucler à vide trop rapidement tant que les
    // étudiants n'ont pas fait le reste du code
    this_thread::sleep_for(milliseconds(500));


    // TODO: C'est ici que vous pouvez ajouter l'attente
    {
      unique_lock<mutex> verrou(commun.tableau);
      while((direction==arret)&&(! (verif_mouvement(0,ETAGE_MAX))))
	{
	  commun.attente.wait(verrou);
	}
    }
  }  
}
