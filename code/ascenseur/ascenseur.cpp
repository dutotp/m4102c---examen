#include <iostream>
#include <string>

#include "Partage.hpp"
#include "Serveur.hpp"

#include "Circulation.hpp"

using namespace std;

bool Circulation::etage_demande[ETAGE_MAX+1];

int main(int argc, char* argv[])
{
  if (argc != 1)
  {
    cerr << "Usage : " << argv[0] << endl;
    exit(0);
  }
  Partage commun;
  
  thread circulation( (Circulation(commun)) );
  thread serveur( (Serveur(commun)) );
  serveur.detach();

  // Les deux threads ne terminent jamais, à
  // moins d'éteindre et redémarrer l'ascenseur

  circulation.join();
}
