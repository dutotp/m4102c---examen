#include <iostream>

#include "Serveur.hpp"
#include "Circulation.hpp"
#include "Partage.hpp"

#include <thread>
#include <chrono>

using namespace std;
using namespace std::chrono;
using boost::asio::ip::tcp;

Serveur::Serveur(Partage& commun_) :
  commun(commun_)
//--TODO-- à remplacer/compléter --TODO--/
  , socket_ecoute(commun_.service_es,
                  tcp::endpoint(tcp::v4(), 4567))
{}

//---------------------------------------------------------------------------------/

void Serveur::operator()(void)
{
  int etage = 42;

  // L'attribut privé "commun" ne sert que pour la seconde partie, avec prise en compte de la synchronisation.
  (void) commun;
  
  // on va traiter plusieurs boutons séquentiellement
  while(true) {

    //-TODO-- à compléter                           --TODO--/
  boost::asio::ip::tcp::socket sock(commun.service_es);
    socket_ecoute.accept(sock);

    boost::asio::read(sock, boost::asio::buffer(&etage, sizeof(etage)));

    commun.affichage.lock();
    cout << "Demande d'étage reçue : " << etage << endl;
    commun.affichage.unlock();

    if((etage>=0)&&(etage<=ETAGE_MAX))
      {
        commun.tableau.lock();
	Circulation::etage_demande[etage]=true;
        commun.tableau.unlock();
        commun.attente.notify_one();
      }
    else
      {
	commun.affichage.lock();
	cout << "Demande d'étage refusée, étage invalide." << endl;
	commun.affichage.unlock();
      }

  }
}
