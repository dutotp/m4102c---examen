#ifndef CIRCULATION_HPP_
#define CIRCULATION_HPP_

#include "Partage.hpp"

#include <mutex>
#include <condition_variable>

#define ETAGE_MAX 9

// Classe avec opérateur parenthèses défini
class Circulation
{
public :
  Circulation(Partage& commun_);
  [[noreturn]] void operator()(void);
  static const unsigned int arret = 0;
  static const unsigned int montee = 1;
  static const unsigned int descente = 2;

public :
  static bool etage_demande[ETAGE_MAX+1];
  
private :
  bool verif_mouvement(int debut, int fin);
  Partage& commun;
  int etage;
  unsigned int direction;
};

#endif // CIRCULATION_HPP_
