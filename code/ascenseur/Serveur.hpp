#ifndef SERVEUR_HPP_
#define SERVEUR_HPP_

#include "Circulation.hpp"
#include "Partage.hpp"
#include <boost/asio.hpp>

class Serveur
{
public :
  Serveur(Partage& commun_);

  [[noreturn]] void operator()(void);

private :
  Partage& commun;
  //--TODO-- à compléter                          --TODO--/
  boost::asio::ip::tcp::acceptor socket_ecoute;
  //------------------------------------------------------/
};

#endif // SERVEUR_HPP_
