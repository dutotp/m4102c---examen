#ifndef PARTAGE_HPP_
#define PARTAGE_HPP_

#include <boost/asio.hpp>

class Partage
{
public :
  Partage();

  // Service d'entrée-sortie
  boost::asio::io_service service_es;

  //Variables de Synchronisation
  // TODO
  std::mutex affichage;
  std::mutex tableau;
  std::condition_variable attente;
  
};

#endif
