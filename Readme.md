---
author: Département info
lang: fr
title: 'M4102C - Examen sur machine - 09/04/19'
---

UGA - IUT2 - Département informatique\
DUT Semestre 4 - Programmation Concurrente et Répartie - Exemple
d\'examen

M4102C - Examen sur machine - Durée : 1h (ceci est un extrait) Tous Documents Autorisés - Coeff. 11
===================================================================================================

Consignes (à lire attentivement) {#consignes-à-lire-attentivement .preambule}
--------------------------------

-   **Les consignes changent d\'une année sur l\'autre, surtout
    lisez-les, celles-ci ne sont qu\'un exemple !**
-   Pour cet examen exceptionnellement, on ne vous demande pas de tester
    systématiquement la réussite des appels système.
-   Vous compilerez les programmes C++ à l\'aide des Makefiles qui vous
    sont fournis.

Ascenseur
---------

Cet exercice se trouve dans le répertoire `code`. Cet exercice est en
deux parties, la deuxième (**Circulation**) étant totalement subordonnée
à la première (**Communication**). N\'essayez pas de faire la deuxième
partie sans avoir fini la première, cela n\'aurait aucun sens et donc ne
vous rapporterait aucun point. Si vous butez sur cette première partie,
passez plutôt rapidement au dernier exercice.

### Communication

Le système d\'ascenseur est commandé par plusieurs processus
s\'exécutant en différents endroits. Le programme `ascenseur` se
trouvant dans le sous-répertoire du même nom, sert à gérer le
déplacement de l\'ascenseur à proprement parler. Le programme `bouton`
(dans le sous-répertoire `bouton`) est susceptible de s\'exécuter sur
tous les objets connectés servant à appeler l\'ascenseur, en particulier
les boutons situés sur les paliers à chaque étage, ainsi que les boutons
à l\'intérieur de l\'ascenseur. Nous commencerons par détailler `bouton`
qui est un programme simple.

#### Boutons

Dans ce programme minimaliste, il vous est demandé de compléter
l\'unique fichier `bouton.cpp` pour obtenir le comportement suivant. Le
programme se lance avec un unique argument en ligne de commande, qui
doit être un chiffre entre 0 et 9.

-   Le programme doit se connecter à un service sur votre machine avec
    l\'IP `127.0.0.1` et sur le port `4567`.
-   Le programme doit ensuite écrire sur la socket ainsi obtenue,
    l\'entier passé en argument au programme (contenu dans la variable
    `etage`).

Pour tester votre bouton, un petit script `test_bouton.sh` vous est
fourni. Attention, ce script ne peut fonctionner correctement que si
vous avez également fini une grande partie de la partie suivante sur
l\'ascenseur.

#### Ascenseur

Dans cette première partie, vous serez amené à écrire le code permettant
de recevoir et prendre en compte les appels de tous les boutons.
Exceptionnellement et pour cette partie uniquement, vous pourrez ne pas
implémenter toutes les vérifications sur les accès concurrents aux
variables et aux ressources partagées. Cette protection sera ajoutée
dans la partie suivante.

La structure de ce programme est la suivante. Il y a deux threads
spécifiques lancés par le thread principal. Un premier thread est basé
sur la classe `Serveur` et sert uniquement à recevoir les informations
d\'étages demandés par les différents processus `bouton` qui se
connectent séquentiellement. Un second thread sert à gérer le
déplacement de l\'ascenseur, avec arrêt aux bons étages et ouverture des
portes. Il n\'y aura pas d\'autres threads à exécuter dans cet exercice,
tout est dans ces deux threads.

Vous éditerez donc les fichiers `Serveur.hpp` et `Serveur.cpp` pour
mettre en place le serveur. Chaque client ayant une durée de vie très
courte avec l\'envoi d\'un unique entier, le serveur n\'a qu\'à lire sur
la socket pour chaque client un seul entier. Chaque client fait donc
l\'objet d\'une itération de la boucle infinie du serveur. Une fois
l\'entier reçu, le serveur vérifie que sa valeur est bien valide avant
de mettre à jour le tableau des étages actuellement demandés, puis
retourne finalement se mettre en attente du bouton suivant.

### Circulation

La circulation de l\'ascenseur est quasiment totalement codée déjà dans
les sources fournies. Il y a cependant deux petits problèmes que vous
pouvez constater si vous avez fini la première partie **Communication**
:

-   Le thread de circulation ne s\'arrête pas à vide : l\'ascenseur
    sonne deux fois par secondes, même si il est vide et à l\'arrêt à
    l\'étage.
-   Les accès aux ressources communes sont fait sans aucunes
    restrictions, ce qui peut causer dans certains cas extrêmes des
    problèmes de concurrence. Si par exemple quelqu\'un demande un étage
    alors que les portes sont en train de se refermer, la demande peut
    être effacée par la fermeture des portes et être oubliée totalement.

1.  Il vous est donc demandé dans cette partie d\'ajouter dans tous les
    fichiers (sauf `ascenseur.cpp`) ce qu\'il est nécessaire d\'ajouter
    pour ne plus avoir de risque d\'interférence entre les threads sur
    les différentes ressources partagées. Cette protection des
    ressources ne devra pas changer significativement le comportement
    actuel du programme : si l\'on reçoit une demande au moment de
    fermer les portes par exemple, il ne faut pas rouvrir les portes,
    mais simplement revenir à l\'étage plus tard dans le parcours usuel
    de l\'ascenseur.
2.  Enfin, il faudra arrêter le thread de `Circulation` lorsque
    l\'ascenseur est vide et à l\'arrêt. Lorsqu\'un appel arrive, le
    `Serveur` peut alors le réveiller pour qu\'il redémarre.

Une solution juste doit avoir la même trace que celle contenue dans le
fichier `trace_test_final.txt` lorsque l\'on exécute le script de test
présent dans le répertoire `bouton`. Attention, ceci est une condition
nécessaire mais pas suffisante pour juger si votre exercice est
parfaitement réalisé.
